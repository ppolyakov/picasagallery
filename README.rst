===================
picasagallery
===================

picasagallery - Позволяет организовать галерею в проекте на Django с фотохостингом на Google Picasa

Лицензия MIT.

Зависимости
===========

* django >= 1.3
* django-sekizai
* gdata

Установка
=========

Git::

    $ git clone https://ppolyakov@bitbucket.org/ppolyakov/picasagallery.git
    $ cd picasagallery
    $ python setup.py install


Добавить приложение в ``INSTALLED_APPS``::

    INSTALLED_APPS = (
        ...
        'sekizai',
        'picasagallery',
        ...
    )


Добавить в ``TEMPLATE_CONTEXT_PROCESSORS``::

    TEMPLATE_CONTEXT_PROCESSORS = (
        ...
        'django.core.context_processors.static',
        'sekizai.context_processors.sekizai',
        ...
    )


Добавить переменные в settings::

    PICASAGALLERY_USER = 'your_pisaca_username'
    PICASAGALLERY_PHOTO_THUMBSIZE = '128'
    PICASAGALLERY_PHOTO_IMGMAXSIZE = '1024'
    PICASAGALLERY_ALBUM_THUMBSIZE = '160c'
    
Подробности:
http://habrahabr.ru/post/139871/

