from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'picasagallery.views.gallery'),
    url(r'^album/(?P<album_id>\d+)/$', 'picasagallery.views.album_list'),
)
